﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobInterviewSample.AbstractClasses
{
    public class InheritanceFromAbstractClass : OverLoadedAbstractClass
    {
        //override Sum() function
        public int Sum() {
            return 200 + 200 ;
        }
    }
}
