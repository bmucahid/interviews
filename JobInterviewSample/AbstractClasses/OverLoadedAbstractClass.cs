﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobInterviewSample.AbstractClasses
{
    public abstract class OverLoadedAbstractClass
    {
        public OverLoadedAbstractClass() { }
        private string PrivateString { get; set; }

        // you can't get gett or sett when readonly is used.
        private readonly string PrivateReadOnlyString;
        public int Sum() {
            return 1 + 1;
        }
        public int Sum(int a, int b)
        {
            return a + b;
        }
        //overload sum() function
        public int Sum(int a, int b,int c)
        {
            return a + b + c;
        }
    }
}
