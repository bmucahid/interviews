﻿
namespace JobInterviewSample.ConsoleMessages
{
    public interface IMessages
    {
        void singletonMessage();
        void dependencyInjectionMessage();
        void abstractClassMessage();
        void genericMessage();
        void reflectionMessage();
        void openClosedMessage();
        void liskovSubstutionMessage();
        void conconstAndReadonlyMessage();
    }
}