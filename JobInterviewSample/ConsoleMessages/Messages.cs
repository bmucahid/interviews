﻿using JobInterviewSample.AbstractClasses;
using JobInterviewSample.ConstAndReadonly;
using JobInterviewSample.DependencyInjection;
using JobInterviewSample.DependencyInjection.VehicleType;
using JobInterviewSample.Generics;
using JobInterviewSample.Reflections;
using JobInterviewSample.SingletonPattern;
using JobInterviewSample.SOLID.LiskovSubstitution;
using JobInterviewSample.SOLID.OpenClosed;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobInterviewSample.ConsoleMessages
{
    public class Messages : IMessages
    {
      
        public Messages() {
           
        }
        public void openClosedMessage()
        {
            Console.WriteLine("...OpenClosedPrecipleMessage start... \n");
            AreaCalculator areaCalculator = new AreaCalculator();
            var shapes = new List<Shape>{
                new Rectangle{Height=4,Width=6},
                new Circle{Radius=3}
            };
            var areas = new List<double>();
            foreach (Shape shape in shapes)
            {
                areas.Add(shape.Area());
            }
            Console.WriteLine("Area Rectangle :" + areas[0].ToString());
            Console.WriteLine("Area Circle :" + areas[1].ToString());
            Console.ReadLine();
        }
        public void liskovSubstutionMessage()
        {
            Console.WriteLine("...LiskovSubstutionMessage start... \n");
            WarPlane warPlane = new WarPlane();
            ReconnaissanceAircraft reconnaissanceAircraft = new ReconnaissanceAircraft();
            warPlane.MakeDiscovery();
            warPlane.ShootTarget();
            reconnaissanceAircraft.MakeDiscovery();
            Console.ReadLine();
        }

        public void abstractClassMessage()
        {
            Console.WriteLine("...abstractClassMessage start... \n");
            InheritanceFromAbstractClass IC = new InheritanceFromAbstractClass();
            //Incoming collection with InheritanceFromAbstractClass
            Console.WriteLine("InheritanceFromAbstractClass override Sum:" + IC.Sum());
            //Incoming collection with OverLoadedAbstractClass
            Console.WriteLine("OverLoadedAbstractClass Sum:" + IC.Sum(12, 22));

            //wrong use. private variable is not called
            //IC.PrivateReadOnlyString;         
            Console.ReadLine();
        
        }

        public void dependencyInjectionMessage()
        {
            Console.WriteLine("...dependencyInjectionMessage start... \n");
            Vehicle car = new Vehicle(new Car());
            car.Drive();
            Vehicle moto = new Vehicle(new Motorcycle());
            moto.Drive();
            Vehicle bus = new Vehicle(new Bus());
            bus.Drive();
            Console.ReadLine();
         
        }

        public void genericMessage()
        {
            Console.WriteLine("...genericMessage start... \n");
            List<string> lststring = new List<string>();
            lststring.Add("1. Record");
            lststring.Add("2. Record");
            List<string> lststring2 = new List<string>();

            GenericMethode gt = new GenericMethode();
            gt.Copy(lststring, lststring2);

            for (int i = 0; i < lststring2.Count; i++)
            {
                Console.WriteLine("GenericMethode Result :" + lststring2[i].ToString());
            }
            Console.ReadLine();
        
        }

      

        public void reflectionMessage()
        {
            Console.WriteLine("...reflectionMessage start... \n");
            ReflectionMethode reflection = new ReflectionMethode();
            reflection.getMethode();
            Console.ReadLine();
       
        }

        public void singletonMessage()
        {
            Console.WriteLine("...singletonMessage start... \n");
           // This prevents the creation of objects with new.
            
            Singleton object1 = Singleton.CreateObject();
            Singleton object2 = Singleton.CreateObject();
            if (object1 == object2)
            {
                Console.WriteLine("Object1 equal Object2");
                Console.WriteLine("Object1 address: " + object1.GetHashCode().ToString());
                Console.WriteLine("Object2 address: " + object2.GetHashCode().ToString());
            }
            Console.ReadLine();
        
        }
        public void conconstAndReadonlyMessage()
        {
            Console.WriteLine("...constMessage start... \n");
            Const varConst = new Const();
            Console.WriteLine(varConst.TestConst());
             
            ReadOnly readOnly = new ReadOnly("Full Name");
            readOnly.writeConsole();
            //wrong use
            //readOnly.compileTime="";
            Console.ReadLine();
        }
    }
}
