﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobInterviewSample.ConstAndReadonly
{
    public class Const
    {
        //The correct form would be:
        public const string myConstant = "Constants are cool!";
        //wrong use
        //public Const(string c) {
        //    myConstant = c;
        //}

        //wrong use
        //const string myConstant2;
        public string TestConst()
        {
            return myConstant;
        }
        public void writeConsole()
        {
            Console.WriteLine($"Name : {myConstant}");
        }
    }
  
}
