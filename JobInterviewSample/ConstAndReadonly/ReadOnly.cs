﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobInterviewSample.ConstAndReadonly
{
    public class ReadOnly
    {
        public readonly string compileTime = "Whateeeva";
        public readonly string runTime;
        public readonly string FullName;
        public ReadOnly(string name)
        { FullName = name; }
        public void writeConsole() {
            Console.WriteLine($"Name : {FullName}");
        }
        public string Testdeneme() {
            return compileTime;
        }
    }
}
