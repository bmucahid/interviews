﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobInterviewSample.DependencyInjection
{
    interface IVehicle
    {
        void Gas();
        void Brake();
        void RightSignal();
        void LeftSignal();
    }
}
