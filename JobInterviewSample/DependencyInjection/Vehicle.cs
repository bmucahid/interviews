﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobInterviewSample.DependencyInjection
{
    class Vehicle
    {
        IVehicle _vehicle;
        public Vehicle(IVehicle vehicle)
        {
            _vehicle = vehicle;
        }
        public void Drive()
        {
            _vehicle.Gas();
            _vehicle.Brake();
            _vehicle.LeftSignal();
            _vehicle.RightSignal();
        }
    }
}
