﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobInterviewSample.DependencyInjection.VehicleType
{
    class Bus : IVehicle
    {
        public void Brake()
        {
            Console.WriteLine("Bus Brake");
        }

        public void Gas()
        {
            Console.WriteLine("Bus Gas");
        }

        public void LeftSignal()
        {
            Console.WriteLine("Bus LeftSignal");
        }

        public void RightSignal()
        {
            Console.WriteLine("Bus RightSignal");
        }
    }
}
