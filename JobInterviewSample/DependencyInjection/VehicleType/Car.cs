﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobInterviewSample.DependencyInjection.VehicleType
{
    class Car : IVehicle
    {
        public void Brake()
        {
            Console.WriteLine("Car Brake");
        }

        public void Gas()
        {
            Console.WriteLine("Car Gas");
        }

        public void LeftSignal()
        {
            Console.WriteLine("Car LeftSignal");
        }

        public void RightSignal()
        {
            Console.WriteLine("Car RightSignal");
        }
    }
}
