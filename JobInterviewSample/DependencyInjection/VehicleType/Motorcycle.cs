﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobInterviewSample.DependencyInjection.VehicleType
{
    class Motorcycle : IVehicle
    {
        public void Brake()
        {
            Console.WriteLine("Motorcycle Brake");
        }

        public void Gas()
        {
            Console.WriteLine("Motorcycle Gas");
        }

        public void LeftSignal()
        {
            Console.WriteLine("Motorcycle LeftSignal");
        }

        public void RightSignal()
        {
            Console.WriteLine("Motorcycle RightSignal");
        }
    }
}
