﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobInterviewSample.Generics
{
    public class GenericMethode
    {
        public void Copy<T>(List<T> source, List<T> destination)
        {
            foreach (T obj in source)
            {
                destination.Add(obj);
            }
        }
    }
}
