﻿using JobInterviewSample.AbstractClasses;
using JobInterviewSample.ConsoleMessages;
using JobInterviewSample.SingletonPattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobInterviewSample
{
    class Program
    {
        static void Main(string[] args)
        {
            Messages message =new Messages();
            message.conconstAndReadonlyMessage();
            message.liskovSubstutionMessage();
            message.openClosedMessage();
            message.dependencyInjectionMessage();
            message.abstractClassMessage();
            message.genericMessage();
            message.singletonMessage();
            message.reflectionMessage();
        }
    }
}
