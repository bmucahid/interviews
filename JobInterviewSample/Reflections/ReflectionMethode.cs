﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobInterviewSample.Reflections
{
    class ReflectionMethode
    {
        public void getMethode() {

            Student o = new Student();
            o.GetType().GetMethods().ToList().ForEach(m =>
            {
                if (m.Name == "CalcLessonPoint")
                {
                    var Sonuc = m.Invoke(o, new object[] { 70, 90 });
                    Console.WriteLine("Lesson Point Awg : "+Sonuc);
                }
            });
        }
    }
}
