﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobInterviewSample.Reflections
{
    class Student
    {
        public string StudentNo { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public double CalcLessonPoint(int Point1, double Point2) => (Point1 + Point2)/2;
    }
}
