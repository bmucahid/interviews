﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobInterviewSample.SOLID.LiskovSubstitution
{
    public interface IMakeDiscovery
    {
        bool MakeDiscovery();
    }
}
