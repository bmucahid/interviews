﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobInterviewSample.SOLID.LiskovSubstitution
{
    // Another new plane has arrived in our army, but it can only be explored.
    public class ReconnaissanceAircraft : IMakeDiscovery
    {
        public bool MakeDiscovery()
        {
            Console.WriteLine("Reconnaissance Airplane completed the discovery.");
            return true;
        }
    }
}
