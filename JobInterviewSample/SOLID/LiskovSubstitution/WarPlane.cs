﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobInterviewSample.SOLID.LiskovSubstitution
{
    // Our fighter jet can hit both reconnaissance and target.
    public class WarPlane : IShootTarget, IMakeDiscovery
    {
        public bool MakeDiscovery()
        {
            Console.WriteLine("War Plane completed the discovery.");
            return true;
        }

        public bool ShootTarget()
        {
            Console.WriteLine("Our warplane hit the target.");
            return true;
        }
    }
}
