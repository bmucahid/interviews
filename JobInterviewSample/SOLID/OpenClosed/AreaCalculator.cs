﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobInterviewSample.SOLID.OpenClosed
{
 
    //proper use
    public class AreaCalculator
    {
        public double Area(Shape[] shapes)
        {
            double area = 0;
            foreach (var item in shapes)
            {
                area += item.Area(); 
            }
            return area;
        }
    }
    //wrong use
    //public class AreaCalculator
    //{
    //    public double Area(object[] shapes) {
    //        double area = 0;
    //        foreach (var item in shapes)
    //        {
    //            if (item is Rectangle)
    //            {
    //                Rectangle rec= (Rectangle)item;
    //                area += rec.Height * rec.Width;
    //            }
    //            if(item is Circle)
    //            {
    //                Circle cir = (Circle)item;
    //                area += cir.Radius*cir.Radius * Math.PI;
    //            }
    //        }
    //        return area;
    //    }
    //}
}
