﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobInterviewSample.SOLID.OpenClosed
{
    //wrong use
    //public class Circle
    //{
    //    public double Radius { get; set; }
    //}


    //Proper use
    public class Circle:Shape
    {
        public double Radius { get; set; }

        public override double Area()
        {
            return (Radius * Radius * Math.PI);
        }
    }
}
