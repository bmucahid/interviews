﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobInterviewSample.SOLID.OpenClosed
{
  //proper use
    public class Rectangle :Shape
    {
        public double Width { get; set; }
        public double Height { get; set; }

        public override double Area()
        {
            return (Width * Height);
        }
    }
    //wrong use
    //public class Rectangle 
    //{
    //    public double Width { get; set; }
    //    public double Height { get; set; }
    //}
}
