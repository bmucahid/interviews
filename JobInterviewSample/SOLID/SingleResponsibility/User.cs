﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobInterviewSample.SOLID.SingleResponsibility
{
    public class User
    {
        public void ChangeUserName()
        {

        }

        public void ChangeEmailAddress()
        {

        }
        // It is against the principle of SingleResponsibility. EmailHelper was created for this. any class can use sendAnEmail ().
   
    }
    //wrong use

    //public class User
    //{
    //    public void ChangeUserName()
    //    {

    //    }

    //    public void ChangeEmailAddress()
    //    {

    //    }
    //    // wrong
    //    public void SendAnEmail()
    //    {

    //    }
    //}
}
