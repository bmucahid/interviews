﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobInterviewSample.SingletonPattern
{
    public class Singleton
    {
        private Singleton()
        {

        }
        private static Singleton MyObject;

        //Constructor must be private. This prevents the creation of objects with new.
        //A static member of the same type as Class is created.
        public static Singleton CreateObject()
        {
            if (MyObject == null)
            {
                MyObject = new Singleton();
            }
            return MyObject;
        }
        
    }
}
